package engineTester;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.TexturedModel;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.*;
import models.RawModel;
import shaders.StaticShader;
import terrains.Terrain;
import textures.ModelTexture;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainGameLoop {
    public static void main(String[] args) {
        DisplayManager.createDisplay();
        Loader loader = new Loader();
        StaticShader shader = new StaticShader();

        RawModel model = OBJLoader.loadToOBJ("tree", loader);

        TexturedModel texturedModel = new TexturedModel(model, new ModelTexture(loader.loadTexture("tree")));
        //Entity entity= new Entity(texturedModel, new Vector3f(0, 0, -25), 0, 0, 0, 1);
        Camera camera = new Camera();
        Light light = new Light(new Vector3f(3000, 2000, 2000), new Vector3f(1, 1, 1));

        List<Entity> entities = new ArrayList<Entity>();
        Random random = new Random();
        for(int i=0;i<500;i++){
            entities.add(new Entity(texturedModel, new Vector3f(random.nextFloat()*800 - 400,0,random.nextFloat() * -600),0,0,0,3));
        }


        Terrain terrain = new Terrain(0, -1, loader, new ModelTexture(loader.loadTexture("grass")));
        Terrain terrain2 = new Terrain(-1, -1, loader, new ModelTexture(loader.loadTexture("grass")));

        ModelTexture texture = texturedModel.getModelTexture();
        texture.setShineDamper(10);
        texture.setReflectivity(1);

        MasterRenderer renderer = new MasterRenderer();

        while(!Display.isCloseRequested()){
            //entity.increaseRotation(0, 0.5f, 0);

            camera.move();

            renderer.processTerrain(terrain);
            renderer.processTerrain(terrain2);

            for(Entity entity:entities){
                renderer.processEntity(entity);
            }

            renderer.render(light, camera);
            DisplayManager.updateDisplay();
        }


        renderer.cleanUp();
        loader.cleanUP();
        DisplayManager.closeDisplay();
    }
}
